# RSO Mirror

## Host setup

This file contains the steps you need to do to set up Ansible on your management device, and the OS on your target server, before running these scripts.

### Install ansible on your managment device

These scripts have been tested with [Ubuntu 20.04](https://ubuntu.com/) as the management device that runs Ansible.

To install the required dependencies, run:
```
sudo aptitude install ansible sshpass python3-netaddr
ansible-galaxy collection install community.general
```

Connect to your target server over what you plan to be the WAN interface.

### Install CentOS on target

These scripts have been tested with [CentOS 8 stream](https://www.centos.org/centos-stream/).

We recommend that there be at least one SSD in the server. Having `/data` mounted to the SSD will improve performance when writing installations using Proins. 

The server does not need to be installed to the SSD, instead we recommend the SSD be partitioned using `gpt` and be formatted using optimally aligned partitions as `ext4` and mounted to `/data` using `/etc/fstab`. Be sure to set `defaults` which should contain `relatime`. You have to do this manually, as this depends on your hardware and the code does not take care of it.

If you wish to further improve performance, you may try using an `ext4` `/data` partition with journaling turned off, but this has not been tested.

On installation choose to create a non-root user with a password and check the administrator (sudo) checkbox.

### Add target to your inventory

`inventory-production` is a submodule and is private. It is recommended that you build your own production inventory from `inventory-example`.

Add your target to your inventory under [main.yml](inventory-example/main.yml). 

Set `ansible_user` to your non-root sudo user. Be sure to fill out the remaining parameters according to the example. Fill out the [HQ configuration](inventory-example/rsohq.yml). We recommend that you encrypt any passwords using vault, the command example is provided in the files.

Take a look at the [global group variables](inventory-example/group_vars/all.yml) for any settings you might want to change. To define them per target server, you may redefine them in your [host_vars](inventory-example/host_vars).
