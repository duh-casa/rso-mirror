# RSO HQ

The HQ server is the server which centrally runs the [eRSO application](https://gitlab.com/duh-casa/erso) (on a firewalled LAN) and hosts the [Proins](https://gitlab.com/duh-casa/proins) OS files and OS images for it's use.

Since in the case of the RSO project here in Slovenia we only ever needed to set up this server once, there is no automation for setting it up. However this file will at least document what is required, if you are setting up a similar project elsewhere.

## eRSO application

This application is a simple PHP website that you configure to be accessible from your server LAN.

See https://gitlab.com/duh-casa/erso for details.

Since this application might not have stringent security, it is recommended to configure the site to only be accessible from localhost and a firewalled server LAN.

Ansible will generate a user to use for the SSH tunnel when setting up a mirror.

## Proins

All Proins really requires that isn't OS images is an unprivileged user, which will keep the OS images in it's home directory.

The default username for this is `mirror`. You may change this username in your [global group variables](inventory-example/group_vars/all.yml).

### Proins OS

While Proins itself is open source and can be [downloaded](https://gitlab.com/duh-casa/proins), what you actually use in order to write OS images to the disk is a bootable operating system we are calling Proins OS.

Proins OS boots from NFS and is therefore an entire operating system image (all the files). As such I'm not sure if it could be made available for download in a secure manner.

However, creating your own Proins OS is not complicated: All you need is a desktop Linux operating system, we used Ubuntu 20.04. The OS can be initially installed to a disk. It is recommended that this installation have no swap file.

Do not forget to clone Proins into `/proins`:
```
sudo -i
cd /
git clone https://gitlab.com/duh-casa/proins.git
```

Then follow the procedure documented here:
* https://gitlab.com/duh-casa/RV-disks/-/blob/master/doc/NFS%20boot.md#client-setup

For compatibility with this project, instead of rsyncing your files to `/media/nfs`, you may want to use something like:
```
sudo rsync -axAX -M--fake-super / mirror@hq.rso.net:proins/
```

#### RAM disk Overlay

The instructions above show you how to set the RAM disk Overlay. This is recommended so that simultaneous instances of Proins OS don't interfere with each other. When using a RAM disk Overlay, any changes to Proins OS while it is booted are discarded upon shutdown.

Therefore you might want to have one USB key without it, so that you can make changes to Proins OS. To do so, simply delete the overlay files and do `initramfs -u`. Be careful that `initramfs -u` is actually updating for the kernel version your `grub.cfg` is using.

#### Bugs

At present, `update-grub` is broken in Proins OS and cannot be used. You might want to upgrade grub and the kernel on the source disk and then re-upload it.

### OS images

#### Generating

The best way to generate OS images is to boot Proins OS and use the Proins scripts:
https://gitlab.com/duh-casa/proins/-/blob/master/mkimage/mkimage.sh

It is recommended to remove any swap files from the OS before generating the OS image. Proins will generate swap when writing the image.

If the written image has issues because of special file permissions that rsync did not set up correctly (dbus usually), you can put a script in `/home/fixes.sh` in your source OS and Proins will implement them when writing the image.

If you are doing this on a mirror, do not forget to upload the images to HQ using the [Mirror managment interface](https://gitlab.com/duh-casa/rso-mirror-management).

#### Writing

To write an OS image, boot Proins OS, and then run the appropriate `supervisor.sh` script with sudo:
* https://gitlab.com/duh-casa/proins/-/blob/master/scripts-express/supervisor.sh
* https://gitlab.com/duh-casa/proins/-/blob/master/scripts-express-efi/supervisor.sh

The script takes two parameters, the first is the name of the image to write, the second is the UUID of the source disk -- which is no longer required to be correct.

The script interactively determines the disk to write the image to.

You can create a `.desktop` icon for running this command to simplify usage. There seems to be some kind of bug in GNOME in making the icons executable, so if they don't work on the desktop, copy them to `~/.local/share/applications` and they will run fine from Dash.
