# RSO Mirror

This is automation required to set up a RSO mirror -- a server to be dispatched to a RSO project subsidiary to be able to use our software:
* https://gitlab.com/duh-casa/erso
* https://gitlab.com/duh-casa/proins

## Description

### [host-init](roles/host-init)

Initialize the host:
* Sets the hostname to match the one from the inventory.
* Sets up private key login (after this you can drop `--ask-pass`)
* Disables root SSH login if ansible_user is not root
* Removes access to cockpit from Public interface

### [router](roles/router)

Sets up the device as a router (requires two network interfaces):
* Sets up the LAN network interface
* Sets up IP forwarding and NAT
* Sets up dnsmasq (DHCP and DNS servers)
* Configures firewall zone "internal"

### [gateway](roles/gateway)

Sets up a gateway to allow internal users to access remote internal HTTP service:
* Installs autossh
* Sets up private key SSH from target root to HQ user (acceping ECDSA fingerprint)
* Sets up private key SSH from HQ ansible to target ansible (for administration)
* Configures autossh to run as a systemd service
* Adds local DNS record for the service (so that service can be seamlessly accessed)
* Configures firewall to allow LAN access to service but deny public access

### [mirror](roles/mirror)

Sets up the local Proins OS mirror, to enable fast OS image writing:
* Prepares authentication and directories for Proins and images mirroring
* Configures cron jobs that do periodic mirror synchronization
* Sets up NFS server for Proins OS boot
* Sets up local rsync server for Proins installation writing

### [management](roles/management)

Sets up everything needed for the management interface:
* Installs apache2 and php
* Configures apache to (only) be accessible from the internal network
* Configures apache to listen on port 8080
* Deploys the management code from GIT
* Deploys a JSON configuration file containing some Ansible facts

The management code deployed has [it's own README](https://gitlab.com/duh-casa/rso-mirror-management).

### [memtest](roles/memtest)

Sets up PXE boot for [memtest86+](http://memtest.org/) in the internal network:
* Configures dnsmasq for TFTP boot 
* Downloads [syslinux](https://wiki.syslinux.org/) 
* Generates syslinux configuration and links files into tftpboot directory
* Downloads memtest86+
* Configures firewall

This role requires the [router role](roles/router) above to be installed first.

This role conflicts with the [ipxe role](roles/ipxe) below. You must choose either.

### [ipxe](roles/ipxe)

Sets up PXE boot to enable booting to Proins in the internal network:
* Downloads and compiles iPXE on the ansible controller
* Configures the chainload to HTTP and bundles it with the binary
* Deploys the iPXE binary on the target server
* Configures dnsmasq for TFTP boot
* Configures firewall

This role requires the [router role](roles/router) above to be installed first.

This role conflicts with the [memtest role](roles/memtest) above. You must choose either.

[iPXE](https://ipxe.org/) enables loading the kernel and initramfs over HTTP, which is significantly faster than conventional TFTP, bringing the boot times down to practical speeds (TFTP boot of Proins would take about 20 minutes).

## Installation

This code was tested on Ubuntu 20.04 as a management device OS (runs Ansible) and CentOS 8 stream as the target server OS.

Please note that this code relies on the existence of an already configured [HQ server](RSOHQ.md).

See [SETUP.md](SETUP.md) for recommended setup of management device and target server.

## Usage

### Configuring target server

By default, the connected ethernet port will be configured as the WAN interface and the other one as the LAN interface. You will want to use the faster ethernet port for LAN, as to maximize local image writing speed.

Be sure to accept the ECDSA fingerprint of your server by establishing a SSH connection to it.

Then to run the playbook, simply run:
```
ansible-playbook -i inventory-production playbook.yml --ask-pass
```

If you are using vault in your inventory, put the password into a file called `secret` and append `--vault-id secret` to the command line. If you do not wish to save your password to a file, use `--vault-id prompt` instead.

Please note that you may have to edit the `playbook.yml` in accordance to what you wish to configure and on which host.

### Innitial mirror synchronization

Once the server has been configured, a cron job to synchronize the mirror around midnight would have been present.

If you wish to start the synchronization immediately, you may start it using the [management interface](https://gitlab.com/duh-casa/rso-mirror-management).

Please note that depending on the strength of the link between your target server and the HQ server, as well as the size of your OS images, initial synchronization may take hours or days.

### Proins OS boot

Once the first mirror synchronization has completed, machines connected to the server's LAN interface should be able to boot Proins OS.

Proins contains scripts for generating boot USB keys or boot CD ISOs:
* https://gitlab.com/duh-casa/proins/-/blob/master/iso/generate.sh
* https://gitlab.com/duh-casa/proins/-/blob/master/scripts-usb/supervisor.sh

Depending on your settings of the LAN subnet, the Proins OS files may have to be updated with a capacity to find the local Proins server:
* https://gitlab.com/duh-casa/RV-disks/-/blob/master/proins/etc/grub.d/09_nfs#L143
* https://gitlab.com/duh-casa/proins/-/blob/master/scripts-express/03-rsync.sh#L18

Theoretically, GRUB should be able to use DNS servers from DHCP but this has not been tested.

## Contributing

This software package is created for the RSO project that operates locally in Slovenia. It is made available in open source in the hopes it will be useful, for other people attempting to do something similar. As such we haven't had any need for outside contributions so far. 

If you wish to contribute please contact the existing contributors.

## Licencse

[Affero GPL v3](LICENSE)

## Project status

This code is being used in a production environment and will receive updates as practical usage requires.
